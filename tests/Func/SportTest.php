<?php

namespace App\Tests\Func;

use DateTimeImmutable;

class SportTest extends AbstractApiTest
{
    public function testGetCollection(): void
    {
        $response = static::createClient()->request('GET', '/api/sports', ['headers' => [
            'Accept' => 'application/json',
        ]]);
        $this->assertResponseStatusCodeSame(200);
    }

    public function testPost(): void
    {
        $dataIN = [
            'headers' => [
                'Accept' => '*/*',
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'name' => "unSportTest"
            ]
        ];

        $response = static::createClient()->request('POST', '/api/sports', $dataIN);
        $this->assertResponseStatusCodeSame(401);
    }
}
