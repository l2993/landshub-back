<?php

namespace App\Tests\Func;

use DateTimeImmutable;

class BookingTest extends AbstractApiTest
{
    public function testGetCollection(): void
    {
        $response = static::createClient()->request('GET', '/api/bookings/224', ['headers' => [
            'Accept' => 'application/json',
        ]]);

        $this->assertResponseStatusCodeSame(401);

        $dataIN = [
            'headers' => [
                'Accept' => '*/*',
                'Content-Type' => 'application/json'
            ]
        ];

        $token = $this->getToken("ChristopheHamel@sport.com", "password");
        $dataIN['headers'] = array_merge($dataIN['headers'], ['Authorization' => 'Bearer ' . $token]);

        $this->assertResponseStatusCodeSame(200);
    }

    public function testPost(): void
    {
        $dataIN = [
            'headers' => [
                'Accept' => '*/*',
                'Content-Type' => 'application/json'
            ],
            'json' => [
                "bookedTime" => "2044-09-05 16:00:00",
                "land" => "/api/lands/285"
            ]
        ];

        $response = static::createClient()->request('POST', '/api/bookings', $dataIN);
        $this->assertResponseStatusCodeSame(401);

        $token = $this->getToken("ChristopheHamel@sport.com", "password");
        $dataIN['headers'] = array_merge($dataIN['headers'], ['Authorization' => 'Bearer ' . $token]);

        $response = static::createClient()->request('POST', '/api/bookings', $dataIN);
        $this->assertResponseStatusCodeSame(201);
    }
}
