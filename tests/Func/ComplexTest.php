<?php

namespace App\Tests\Func;

use DateTimeImmutable;

class ComplexTest extends AbstractApiTest
{
    public function testGetCollection(): void
    {
        $response = static::createClient()->request('GET', '/api/complexes/274', ['headers' => [
            'Accept' => 'application/json',
        ]]);
        $this->assertResponseStatusCodeSame(200);
    }

    public function testPost(): void
    {
        $dataIN = [
            'headers' => [
                'Accept' => '*/*',
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'name' => "Complex de test",
                'description' => 'Description d\'un complexe de test',
                'logo' => "Logo de test",
                'address' => '12 Rue du test',
                'city' => 'TestiVille',
                'postal' => "12345",
                'hasChangingRoom' => true
            ]
        ];

        $response = static::createClient()->request('POST', '/api/complexes', $dataIN);
        $this->assertResponseStatusCodeSame(401);

        $token = $this->getToken("ChristopheHamel@sport.com", "password");
        $dataIN['headers'] = array_merge($dataIN['headers'], ['Authorization' => 'Bearer ' . $token]);

        $response = static::createClient()->request('POST', '/api/complexes', $dataIN);
        $this->assertResponseStatusCodeSame(403);

        $token = $this->getToken("MarieWagner@sport.com", "password");
        $dataIN['headers'] = array_merge($dataIN['headers'], ['Authorization' => 'Bearer ' . $token]);

        $response = static::createClient()->request('POST', '/api/complexes', $dataIN);
        $this->assertResponseStatusCodeSame(201);
    }
}
