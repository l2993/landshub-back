<?php

namespace App\Tests\Func;

use DateTimeImmutable;

class ReviewTest extends AbstractApiTest
{
    public function testGetCollection(): void
    {
        $response = static::createClient()->request('GET', '/api/reviews/560', ['headers' => [
            'Accept' => 'application/json',
        ]]);
        $this->assertResponseStatusCodeSame(200);
    }

    public function testPost(): void
    {
        $dataIN = [
            'headers' => [
                'Accept' => '*/*',
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'rate' => 1,
                'message' => 'Un message test',
                'complex' => 'api/complexes/273'
            ]
        ];

        $response = static::createClient()->request('POST', '/api/reviews', $dataIN);
        $this->assertResponseStatusCodeSame(401);

        $token = $this->getToken("ChristopheHamel@sport.com", "password");
        $dataIN['headers'] = array_merge($dataIN['headers'], ['Authorization' => 'Bearer ' . $token]);

        $response = static::createClient()->request('POST', '/api/reviews', $dataIN);
        $this->assertResponseStatusCodeSame(201);
    }
}
