<?php

namespace App\Tests\Func;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\ORMException;
use Faker\Factory;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

abstract class AbstractApiTest extends ApiTestCase
{
    protected $em;
    protected $passwordEncoder;
    protected $faker;
    protected $security;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        self::bootKernel();
        $this->em = $this->createMock(EntityManagerInterface::class);
        $this->passwordEncoder = $this->createMock(PasswordHasherInterface::class);
        $this->faker = $faker = Factory::create('fr_FR');
        $this->security = $this->createMock(Security::class);
    }

    protected function addUser(
        array $roles = ["ROLE_PLATER"],
        string $password = "password",
        string $email = "test@sport.com",
        string $firstname = "FirstnameTesting",
        bool $isPublic = true,
        string $avatarUrl = "https://avatars.dicebear.com/api/micah/Test.svg",
        string $phone = "0611223344",
        string $lastname = "LastnameTesting"
    ): User {

        $user = new User();
        $user
            ->setEmail($email)
            ->setFirstName($firstname)
            ->setLastName($lastname)
            ->setRoles($roles)
            ->setIsPublic($isPublic)
            ->setAvatarUrl($avatarUrl)
            ->setPhone($phone)
            ->setPassword($this->passwordEncoder->hash($password));

        $this->save($user);
        return $user;
    }

    protected function save($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    protected function getTokenCreate(array $roles = ["ROLE_USER"], string $mail = "", string $pass = ""): ?string
    {
        if ($mail == "") {
            $mail = $this->faker->email;
        }
        if ($pass == "") {
            $pass = $this->faker->password(6, 20);
        }



        $this->addUser($roles, $pass, $mail);
        return $this->getToken($mail, $pass);
    }

    protected function getToken(string $mail = "", string $pass = ""): ?string
    {
        $dataIN = ['json' => [
            'username' => $mail,
            'password' => $pass
        ]];


        $response = static::createClient()->request('POST', '/api/login_check', $dataIN);
        $dataOUT = json_decode($response->getContent());
        $this->assertResponseStatusCodeSame(200);

        return $dataOUT->token;
    }
}
