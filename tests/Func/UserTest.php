<?php

namespace App\Tests\Func;

use DateTimeImmutable;

class UserTest extends AbstractApiTest
{
    public function testGetCollection(): void
    {
        $response = static::createClient()->request('GET', '/api/users?page=1', ['headers' => [
            'Accept' => 'application/json',
        ]]);
        $this->assertResponseStatusCodeSame(401);
    }

    public function testPost(): void
    {

        $dataIN = [
            'headers' => [
                'Accept' => 'application/json',
            ],
            'json' => [
                'email' => $this->faker->email(),
                'password' => $this->faker->password(),
                'firstname' => $this->faker->firstName(),
                'lastname' => $this->faker->lastName(),
                'phone' => $this->faker->phoneNumber(),
                'roles' => ["ROLE_PLAYER"],
                'isPublic' => true,
                'avatarUrl' => 'urlAvatar'
            ]
        ];

        $response = static::createClient()->request('POST', '/api/users', $dataIN);
        $this->assertResponseStatusCodeSame(201);
    }
}
