# LandsHub - Back :fire:

**LandsHub** est une application web permettant de pouvoir réserver des terrains adaptés à divers sports. Choisissez votre **sport**, votre **salle** et vos **horaires** et c’est parti ! :muscle:

# :cd: Installation :cd:

### Git

Cloner le repository de l'API :
> **Note:** Ce projet est en privé, il se peut que vous n'ayez pas droits pour le cloner.
```
git clone https://gitlab.com/l2993/landshub-back.git
```
### Composer
Installer les librairies Composer :
> **Note:** Assurez vous d'être dans le dossier du projet pour effectuer les commandes suivantes.
```
composer update
```
```
composer install
```

### Doctrine
Créer la base de données et importer le jeu de fausses données
```
php bin/console doctrine:database:create --if-not-exists
```
```
php bin/console doctrine:schema:update --force
```
```
php bin/console doctrine:fixtures:load -n
```

## :open_file_folder: Démarrer l'API :open_file_folder:

Pour lancer l'API, exécuter la commande suivante :
> **Note:** Assurez vous d'être dans le dossier du projet pour effectuer les commandes suivantes.
```
php -S 127.0.0.1:8080 -t public -a
```
> **Note:** Vérifier que le port 8080 ne soit pas déjà utilisé par un autre service


_Ce projet dispose d'une license OpenSource._

## [](#creative-commons)Creative Commons

#### [](#cc0)CC0

[![License: CC0-1.0](https://user-content.gitlab-static.net/0c28359c0320f214681e19173ae15ea36fe5a8ee/68747470733a2f2f6c6963656e7365627574746f6e732e6e65742f6c2f7a65726f2f312e302f38307831352e706e67)](http://creativecommons.org/publicdomain/zero/1.0/)  
`[![License: CC0-1.0](https://licensebuttons.net/l/zero/1.0/80x15.png)](http://creativecommons.org/publicdomain/zero/1.0/)`