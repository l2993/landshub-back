<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Booking;
use App\Entity\User;
use App\Repository\BookingRepository;
use Symfony\Component\Security\Core\Security;

class BookingSearchDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    private $repository;

    public function __construct(BookingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Booking::class === $resourceClass && ($operationName == Booking::$OPERATION_GET);
    }

    /**
     * @inheritDoc
     */
    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): array
    {
        $filteredBooking = $this->repository->findByFilters($context['filters']);
        return $filteredBooking;
    }
}
