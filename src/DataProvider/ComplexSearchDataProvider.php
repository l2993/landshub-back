<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Complex;
use App\Repository\ComplexRepository;
use Symfony\Component\Security\Core\Security;

class ComplexSearchDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    private $repository;

    public function __construct(ComplexRepository $repository)
    {
        $this->repository = $repository;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Complex::class === $resourceClass && ($operationName == Complex::$OPERATION_GET);
    }

    /**
     * @inheritDoc
     */
    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): array
    {
        $filteredComplexes = $this->repository->findByFilters($context['filters']);
        return $filteredComplexes;
    }
}
