<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class Mailer
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendEmail(User $user, string $subject, string $template, array $context)
    {
        $email = (new TemplatedEmail())
            ->from('contact.landshub@gmail.com')
            ->to($user->getEmail())
            ->subject($subject)
            ->htmlTemplate('mails/' . $template . '.html.twig')
            ->context($context);

        $this->mailer->send($email);
    }
}
