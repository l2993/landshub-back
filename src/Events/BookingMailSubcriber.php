<?php

namespace App\Events;

use App\Entity\Booking;
use Symfony\Component\Security\Core\Security;
use App\Service\Mailer;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BookingMailSubcriber implements EventSubscriberInterface
{
    private $security;
    private $mailer;

    public function __construct(Security $security, Mailer $mailer)
    {
        $this->security = $security;
        $this->mailer = $mailer;
    }


    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['setUserBooking', EventPriorities::POST_WRITE]
        ];
    }

    public function setUserBooking(ViewEvent $event)
    {
        $booking = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        $user = $this->security->getUser();

        if ($booking instanceof Booking && $method === "POST") {
            $this->mailer->sendEmail(
                $user,
                "LandsHub | Confirmation de votre réservation !",
                "reservation_accepted",
                ["booking" => $booking,"user" => $user]
            );
        }

        if ($booking instanceof Booking && $method === "PATCH") {
            if ($booking->getIsCancelled() === true) {
                foreach ($booking->getParticipantList() as $participant) {
                    $this->mailer->sendEmail(
                        $participant,
                        "LandsHub | Annulation de votre réservation !",
                        "reservation_canceled",
                        ["booking" => $booking,"user" => $participant]
                    );
                }
            }
        }
    }
}
