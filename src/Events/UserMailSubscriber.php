<?php

namespace App\Events;

use App\Entity\User;
use App\Service\Mailer;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserMailSubscriber implements EventSubscriberInterface
{
    /**
     * Service mailer
     *
     * @var [Mailer]
     */
    private $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['sendEmail', EventPriorities::POST_WRITE]
        ];
    }

    public function sendEmail(ViewEvent $event)
    {
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if ($user instanceof User && $method === "POST") {
            $this->mailer->sendEmail(
                $user,
                "Bienvenue sur LandsHub !",
                "register",
                ["username" => $user->getFirstname()]
            );
        }
    }
}
