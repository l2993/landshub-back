<?php

namespace App\Events;

use App\Entity\Review;
use App\Entity\Complex;
use App\Entity\Friendship;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ReviewInitSubcriber implements EventSubscriberInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }


    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['setUserReview', EventPriorities::POST_VALIDATE]
        ];
    }

    public function setUserReview(ViewEvent $event)
    {
        $complex = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        $user = $this->security->getUser();

        if ($complex instanceof Review && $method === "POST") {
            $complex->setReviewer($user);
        }
    }
}
