<?php

namespace App\Events;

use App\Entity\Complex;
use App\Service\Mailer;
use App\Entity\Friendship;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FriendShipMailSubscriber implements EventSubscriberInterface
{
    /**
     * Service mailer
     *
     * @var [Mailer]
     */
    private $mailer;
    private $security;

    public function __construct(Mailer $mailer, Security $security)
    {
        $this->mailer = $mailer;
        $this->security = $security;
    }


    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['setUserFriendShip', EventPriorities::POST_WRITE]
        ];
    }

    public function setUserFriendShip(ViewEvent $event)
    {
        $friendship = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        $user = $this->security->getUser();

        if ($friendship instanceof Friendship && $method === "POST") {
            $this->mailer->sendEmail(
                $friendship->getReceiver(),
                "LandsHub | Nouvelle demande d'amis !",
                "friend_invitation",
                ["friendship" => $friendship]
            );
        }
    }
}
