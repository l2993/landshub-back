<?php

namespace App\Events;

use App\Entity\Complex;
use App\Entity\Friendship;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FriendShipInitSubcriber implements EventSubscriberInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }


    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['setUserFriendShip', EventPriorities::POST_VALIDATE]
        ];
    }

    public function setUserFriendShip(ViewEvent $event)
    {
        $friendship = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        $user = $this->security->getUser();

        if ($friendship instanceof Friendship && $method === "POST") {
            $friendship->setSender($user);
            $friendship->setIsFriend(false);
        }
    }
}
