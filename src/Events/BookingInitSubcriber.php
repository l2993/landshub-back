<?php

namespace App\Events;

use Exception;
use App\Entity\Booking;
use App\Service\Mailer;
use App\Repository\BookingRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;

class BookingInitSubcriber implements EventSubscriberInterface
{
    private $security;
    private $mailer;
    private $repos;

    public function __construct(Security $security, Mailer $mailer, BookingRepository $repos)
    {
        $this->security = $security;
        $this->mailer = $mailer;
        $this->repos = $repos;
    }


    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['setUserBooking', EventPriorities::POST_VALIDATE]
        ];
    }

    public function setUserBooking(ViewEvent $event)
    {
        $booking = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        $user = $this->security->getUser();

        if ($booking instanceof Booking && $method === "POST") {
            if (!$this->repos->resamax($booking->getBookedTime(), $user->getId())) {
                $booking->addParticipant($user);
                $booking->setIsCancelled(false);
            } else {
                throw new Exception("Too many bookings for today (more than 4)");
            }
        }
    }
}
