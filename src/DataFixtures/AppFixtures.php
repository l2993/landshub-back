<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Land;
use App\Entity\User;
use App\Entity\Coach;
use App\Entity\Sport;
use App\Entity\Review;
use App\Entity\Booking;
use App\Entity\Complex;
use App\Entity\UserSport;
use App\Entity\Friendship;
use App\Entity\LandSchedule;
use App\Entity\ComplexPicture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordHasherInterface
     */
    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        $sports = ["Tennis", "Badminton", "Handball", "Basketball"];
        $niveau = ["Amateur", "Intermédiaire", "Avancé", "Compétiteur"];
        $days = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"];
        $sportArray = [];
        $complexArray = [];
        $userArray = [];
        $bookingArray = [];

        for ($i = 0; $i < sizeof($sports); $i++) {
            $sport = new Sport();
            $sport->setName($sports[$i]);
            $sport->setDescription($faker->text());
            $manager->persist($sport);
            $sportArray[] = $sport;
        }

        for ($i = 0; $i < 20; $i++) {
            $complex = new Complex();
            $complex->setName("Complex de " . $faker->name());
            $complex->setDescription($faker->text());
            $complex->setLogo("https://picsum.photos/id/" . rand(200, 1000) . "/" .
                rand(200, 1000) . "/" . rand(200, 1000));
            $complex->setAddress($faker->streetAddress());
            $complex->setPostal($faker->postcode());
            $complex->setCity($faker->city());
            $complex->setLongitude($faker->longitude());
            $complex->setLatitude($faker->latitude());
            $complex->setHasChangingRoom((bool)random_int(0, 1));
            $complexArray[] = $complex;

            for ($p = 0; $p < rand(1, 5); $p++) {
                $complexPictures = new ComplexPicture();
                $complexPictures->setUrl("https://picsum.photos/id/" . rand(200, 1000) . "/" .
                    rand(200, 1000) . "/" . rand(200, 1000));
                $complexPictures->setComplex($complex);
                $manager->persist($complexPictures);
            }
            for ($q = 0; $q < rand(1, 3); $q++) {
                $land = new Land();
                $land->setName("terrain " . $faker->lastName());
                $land->setPrice(rand(1, 100));
                $land->setIsInside((bool)random_int(0, 1));
                $land->setHasEquipments((bool)random_int(0, 1));
                $land->setSport($sportArray[$q]);
                $land->setComplex($complex);
                $manager->persist($land);

                for ($w = 0; $w < sizeof($days); $w++) {
                    $landschedule = new LandSchedule();
                    $landschedule->setLand($land);
                    $landschedule->setDay($days[$w]);
                    $landschedule->setStartingTimeMorning(rand(7, 9));
                    $landschedule->setEndingTimeMorning(rand(11, 12));
                    $landschedule->setStartingTimeAfternoon(rand(12, 14));
                    $landschedule->setEndingTimeAfternoon(rand(18, 23));
                    $manager->persist($landschedule);
                }

                for ($o = 0; $o < rand(0, 5); $o++) {
                    $booking = new Booking();
                    $booking->setLand($land);
                    $date = $faker->dateTime();
                    $date->setTime(rand(14, 18), 0);
                    $booking->setBookedTime($date);

                    if ($faker->boolean(80)) {
                        $booking->setIsCancelled(false);
                    } else {
                        $booking->setIsCancelled(true);
                    }

                    $bookingArray[] = $booking;
                }
            }
        }

        for ($i = 0; $i < 20; $i++) {
            $user = new User();
            $user->setEmail($faker->firstname() . $faker->lastName() . "@sport.com");
            $user->setPassword($this->encoder->hashPassword($user, "password"));
            $user->setFirstname($faker->firstName());
            $user->setLastname($faker->lastName());
            $user->setPhone($faker->phoneNumber());
            $user->setIsPublic((bool)random_int(0, 1));
            $user->setAvatarUrl("https://avatars.dicebear.com/api/micah/" . $faker->firstname() . ".svg");

            if ($faker->boolean(20)) {
                $user->setRoles(["ROLE_MANAGER"]);
                $user->addManager($faker->randomElement($complexArray));
            } else {
                $user->setRoles(["ROLE_PLAYER"]);
            }

            $sports = $faker->randomElements($sportArray, $faker->numberBetween(2, 4));
            foreach ($sports as $sport) {
                $usersport = new UserSport();
                $usersport->setSport($sport);
                $usersport->setLevel($niveau[rand(0, sizeof($niveau) - 1)]);

                $user->addUserSport($usersport);

                $manager->persist($usersport);
            }

            $userArray[] = $user;

            for ($p = 0; $p < rand(1, 3); $p++) {
                $review = new Review();
                $review->setRate(rand(1, 5));
                $review->setMessage($faker->text());
                $review->setComplex($complexArray[rand(0, sizeof($complexArray) - 1)]);
                $review->setReviewer($user);
                $manager->persist($review);
            }

            $manager->persist($user);
        }

        for ($b = 0; $b < sizeof($bookingArray); $b++) {
            $participants = $faker->randomElements($userArray, $faker->numberBetween(1, 6));
            for ($g = 0; $g < sizeof($participants); $g++) {
                $bookingArray[$b]->addParticipant($participants[$g]);
            }

            $manager->persist($bookingArray[$b]);
        }
        for ($i = 0; $i < 10; $i++) {
            $friends = $faker->randomElements($userArray, 2);

            $friendship = new Friendship();
            $friendship->setIsFriend(rand(0, 1));
            $friendship->setSender($friends[0]);
            $friendship->setReceiver($friends[1]);

            $manager->persist($friendship);
        }

        $users = $faker->randomElements($userArray, 5);
        foreach ($users as $user) {
            $coach = new Coach();
            $coach->setEmail($user->getEmail());
            $coach->setPassword($user->getPassword());
            $coach->setFirstname($user->getFirstname());
            $coach->setLastname($user->getLastname());
            $coach->setPhone($user->getPhone());
            $coach->setIsPublic($user->getIsPublic());
            $coach->setPrice(rand(20, 100));
            $coach->setDescription($faker->text());
            $coach->setAvatarUrl("https://avatars.dicebear.com/api/micah/" . $faker->firstname() . ".svg");


            $coachSports = $faker->randomElements($user->getUserSports(), $faker->numberBetween(1, 2));
            foreach ($coachSports as $sport) {
                $coach->addSport($sport->getSport());
            }

            $manager->persist($coach);
        }

        foreach ($complexArray as $complex) {
            $users = $faker->randomElements($userArray, $faker->numberBetween(1, 3));
            foreach ($users as $user) {
                $complex->addUser($user);
                $manager->persist($complex);
            }
        }

        $manager->flush();
    }
}
