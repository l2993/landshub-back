<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BookingRepository;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     formats={"json"},
 *     denormalizationContext={"groups"={"Booking:write"}},
 *     normalizationContext={"groups"={"Booking:read"}},
 *     attributes={"esql"=true}
 * )
 * @ORM\Entity(repositoryClass=BookingRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(callback="checkBooking", groups={"create"})
 */
class Booking
{
    public static $OPERATION_GET = "get";

    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"Booking:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({
     *     "Booking:read",
     *     "Land:read",
     *     "Booking:write"
     * })
     */
    private $bookedTime;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="bookings")
     * @Groups({
     *     "Booking:read",
     *     "Booking:write",
     *     "Land:read"
     * })
     */
    public $participants;

    /**
     * @ORM\ManyToOne(targetEntity=Land::class, inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     "Booking:read",
     *     "Booking:write"
     * })
     */
    private $land;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({
     *     "Booking:read",
     *     "Land:read",
     *     "Booking:write"
     * })
     */
    private $isCancelled;

    public function __construct()
    {
        $this->participants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBookedTime(): ?\DateTimeInterface
    {
        return $this->bookedTime;
    }

    public function setBookedTime(\DateTimeInterface $bookedTime): self
    {
        $this->bookedTime = $bookedTime;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function addParticipant(User $participant): self
    {
        if (!$this->participants->contains($participant)) {
            $this->participants[] = $participant;
        }

        return $this;
    }

    public function getParticipantList(): array
    {
        $participants = [];
        foreach ($this->participants as $participant) {
            $participants[] = $participant;
        }
        return $participants;
    }

    public function removeParticipant(User $participant): self
    {
        $this->participants->removeElement($participant);

        return $this;
    }

    public function getLand(): ?Land
    {
        return $this->land;
    }

    public function setLand(?Land $land): self
    {
        $this->land = $land;

        return $this;
    }

    public function getIsCancelled(): ?bool
    {
        return $this->isCancelled;
    }

    public function setIsCancelled(bool $isCancelled): self
    {
        $this->isCancelled = $isCancelled;

        return $this;
    }
}
