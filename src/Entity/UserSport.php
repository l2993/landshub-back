<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserSportRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     formats={"json"},
 *     normalizationContext={"groups"={"UserSport:read"}},
 *     denormalizationContext={"groups"={"UserSport:write"}},
 *     collectionOperations={
 *         "post"={}
 *     },
 *     itemOperations={
 *         "get"={},
 *         "patch"={},
 *         "delete"={},
 *     }
 * )
 * @ORM\Entity(repositoryClass=UserSportRepository::class)
 */
class UserSport
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"Coaching:read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userSports")
     * @ORM\JoinColumn(nullable=false)
     */
    public $player;

    /**
     * @ORM\ManyToOne(targetEntity=Sport::class, inversedBy="userSports")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *      "Coaching:read",
     *      "User:read",
     *      "UserSport:read",
     *      "UserSport:write",
     *      "User:write"
     * })
     */
    private $sport;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "Coaching:read",
     *     "User:read",
     *     "UserSport:write",
     *     "User:write"
     * })
     */
    private $level;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlayer(): ?User
    {
        return $this->player;
    }

    public function setPlayer(?User $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getSport(): ?Sport
    {
        return $this->sport;
    }

    public function setSport(?Sport $sport): self
    {
        $this->sport = $sport;

        return $this;
    }

    public function getLevel(): ?string
    {
        return $this->level;
    }

    public function setLevel(string $level): self
    {
        $this->level = $level;

        return $this;
    }
}
