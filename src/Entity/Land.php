<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\LandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     formats={"json"},
 *     normalizationContext={"groups"={"Land:read"}},
 *     denormalizationContext={"groups"={"Land:write"}},
 * )
 * @ORM\Entity(repositoryClass=LandRepository::class)
 */
class Land
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({
     *     "Land:read",
     *     "Booking:read",
     *     "Complex:read",
     *     "LandSchedule:read",
     *     "Sport:read"
     * })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"Land:read",
     * "Booking:read",
     * "Complex:read",
     * "LandSchedule:read",
     * "Sport:read",
     * "Complex:write",
     * "Land:write"})
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     * @Groups({
     *     "Land:read",
     *     "Booking:read",
     *     "Complex:read",
     *     "Sport:read",
     *     "Complex:write",
     *     "Land:write"
     * })
     */
    private $price;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({
     *     "Land:read",
     *     "Booking:read",
     *     "Complex:read",
     *     "Sport:read",
     *     "Complex:write",
     *     "Land:write"
     * })
     */
    private $isInside;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({
     *     "Land:read",
     *     "Booking:read",
     *     "Complex:read",
     *     "Sport:read",
     *     "Complex:write",
     *     "Land:write"
     * })
     */
    private $hasEquipments;

    /**
     * @ORM\ManyToOne(targetEntity=Sport::class, inversedBy="lands")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     "Land:read",
     *     "Complex:read",
     *     "Complex:write","Land:write"
     * })
     */
    private $sport;

    /**
     * @ORM\ManyToOne(targetEntity=Complex::class, inversedBy="lands")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     "Land:read",
     *     "Booking:read",
     *     "Sport:read","Land:write"
     * })
     */
    public $complex;

    /**
     * @ORM\OneToMany(targetEntity=LandSchedule::class, mappedBy="land", orphanRemoval=true, cascade={"persist"})
     * @Groups({
     *     "Land:read",
     *     "Complex:write",
     *     "Land:write",
     *     "Complex:read"
     * })
     */
    private $landSchedules;

    /**
     * @ORM\OneToMany(targetEntity=Booking::class, mappedBy="land", orphanRemoval=true)
     * @Groups({
     *     "Land:read"
     * })
     */
    private $bookings;

    public function __construct()
    {
        $this->landSchedules = new ArrayCollection();
        $this->bookings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getIsInside(): ?bool
    {
        return $this->isInside;
    }

    public function setIsInside(bool $isInside): self
    {
        $this->isInside = $isInside;

        return $this;
    }

    public function getHasEquipments(): ?bool
    {
        return $this->hasEquipments;
    }

    public function setHasEquipments(bool $hasEquipments): self
    {
        $this->hasEquipments = $hasEquipments;

        return $this;
    }

    public function getSport(): ?Sport
    {
        return $this->sport;
    }

    public function setSport(?Sport $sport): self
    {
        $this->sport = $sport;

        return $this;
    }

    public function getComplex(): ?Complex
    {
        return $this->complex;
    }

    public function setComplex(?Complex $complex): self
    {
        $this->complex = $complex;

        return $this;
    }

    /**
     * @return Collection<int, LandSchedule>
     */
    public function getLandSchedules(): Collection
    {
        return $this->landSchedules;
    }

    public function addLandSchedule(LandSchedule $landSchedule): self
    {
        if (!$this->landSchedules->contains($landSchedule)) {
            $this->landSchedules[] = $landSchedule;
            $landSchedule->setLand($this);
        }

        return $this;
    }

    public function removeLandSchedule(LandSchedule $landSchedule): self
    {
        if ($this->landSchedules->removeElement($landSchedule)) {
            // set the owning side to null (unless already changed)
            if ($landSchedule->getLand() === $this) {
                $landSchedule->setLand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Booking>
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setLand($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->removeElement($booking)) {
            // set the owning side to null (unless already changed)
            if ($booking->getLand() === $this) {
                $booking->setLand(null);
            }
        }

        return $this;
    }
}
