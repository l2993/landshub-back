<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ComplexRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     formats={"json"},
 *     normalizationContext={"groups"={"Complex:read"}},
 *     denormalizationContext={"groups"={"Complex:write"}}
 * )
 * @ORM\Entity(repositoryClass=ComplexRepository::class)
 */
class Complex
{
    public static $OPERATION_GET = "get";

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({
     *     "Complex:read",
     *     "LandSchedule:read",
     *     "Land:read",
     *     "ComplexPicture:read",
     *     "Review:read",
     *     "Sport:read"
     * })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "Booking:read",
     *     "Complex:read",
     *     "LandSchedule:read",
     *     "ComplexPicture:read",
     *     "Land:read",
     *     "Review:read",
     *     "Sport:read",
     *     "Complex:write"
     * })
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Groups({
     *     "Complex:read",
     *     "Land:read",
     *     "Sport:read",
     *     "Complex:write"
     * })
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "Booking:read",
     *     "Complex:read",
     *     "Land:read",
     *     "Review:read",
     *     "Sport:read",
     *     "Complex:write"
     * })
     */
    private $logo;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "Booking:read",
     *     "Complex:read",
     *     "Land:read",
     *     "Sport:read",
     *     "Complex:write"
     * })
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=10)
     * @Groups({
     *     "Booking:read",
     *     "Complex:read",
     *     "Land:read",
     *     "Sport:read",
     *     "Complex:write"
     * })
     */
    private $postal;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "Booking:read",
     *     "Complex:read",
     *     "Land:read",
     *     "Sport:read",
     *     "Complex:write"
     * })
     */
    private $city;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({
     *     "Complex:read",
     *     "Complex:write"
     * })
     */
    private $longitude;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({
     *     "Complex:read",
     *     "Complex:write"
     * })
     */
    private $latitude;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({
     *     "Booking:read",
     *     "Complex:read",
     *     "Land:read",
     *     "Sport:read",
     *     "Complex:write"
     * })
     */
    private $hasChangingRoom;

    /**
     * @ORM\OneToMany(targetEntity=ComplexPicture::class, mappedBy="complex", orphanRemoval=true, cascade={"persist"})
     * @Groups({
     *     "Complex:read",
     *     "Complex:write"
     * })
     */
    private $complexPictures;

    /**
     * @ORM\OneToMany(targetEntity=Review::class, mappedBy="complex", orphanRemoval=true)
     * @Groups({"Complex:read"})
     */
    private $reviews;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="manager")
     * @Groups({"Complex:read"})
     */
    public $users;

    /**
     * @ORM\OneToMany(targetEntity=Land::class, mappedBy="complex", orphanRemoval=true, cascade={"persist"})
     * @Groups({
     *     "Complex:read",
     *     "Complex:write"
     * })
     */
    private $lands;

    public function __construct()
    {
        $this->complexPictures = new ArrayCollection();
        $this->reviews = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->lands = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPostal(): ?string
    {
        return $this->postal;
    }

    public function setPostal(string $postal): self
    {
        $this->postal = preg_replace('/\s+/', '', $postal);

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getHasChangingRoom(): ?bool
    {
        return $this->hasChangingRoom;
    }

    public function setHasChangingRoom(bool $hasChangingRoom): self
    {
        $this->hasChangingRoom = $hasChangingRoom;

        return $this;
    }

    /**
     * @return Collection<int, ComplexPicture>
     */
    public function getComplexPictures(): Collection
    {
        return $this->complexPictures;
    }

    public function addComplexPicture(ComplexPicture $complexPicture): self
    {
        if (!$this->complexPictures->contains($complexPicture)) {
            $this->complexPictures[] = $complexPicture;
            $complexPicture->setComplex($this);
        }

        return $this;
    }

    public function removeComplexPicture(ComplexPicture $complexPicture): self
    {
        if ($this->complexPictures->removeElement($complexPicture)) {
            // set the owning side to null (unless already changed)
            if ($complexPicture->getComplex() === $this) {
                $complexPicture->setComplex(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Review>
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    public function addReview(Review $review): self
    {
        if (!$this->reviews->contains($review)) {
            $this->reviews[] = $review;
            $review->setComplex($this);
        }

        return $this;
    }

    public function removeReview(Review $review): self
    {
        if ($this->reviews->removeElement($review)) {
            // set the owning side to null (unless already changed)
            if ($review->getComplex() === $this) {
                $review->setComplex(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addManager($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeManager($this);
        }

        return $this;
    }

    public function getUsersList(): array
    {
        $users = [];
        foreach ($this->users as $user) {
            $users[] = $user;
        }
        return $users;
    }

    /**
     * @return Collection<int, Land>
     */
    public function getLands(): Collection
    {
        return $this->lands;
    }

    public function addLand(Land $land): self
    {
        if (!$this->lands->contains($land)) {
            $this->lands[] = $land;
            $land->setComplex($this);
        }

        return $this;
    }

    public function removeLand(Land $land): self
    {
        if ($this->lands->removeElement($land)) {
            // set the owning side to null (unless already changed)
            if ($land->getComplex() === $this) {
                $land->setComplex(null);
            }
        }

        return $this;
    }
}
