<?php

namespace App\Entity;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\CoachRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     formats={"json"},
 *     normalizationContext={"groups"={"Coaching:read"}},
 *     collectionOperations={
 *         "get"={},
 *         "post"={}
 *     },
 *     itemOperations={
 *         "get"={},
 *         "patch"={},
 *         "delete"={},
 *     }
 * )
 * @ORM\Entity(repositoryClass=CoachRepository::class)
 */
class Coach extends User
{
    public const ROLE = "COACH";

    /**
     * @ORM\Column(type="float")
     * @Groups({"Coaching:read"})
     */
    private $price;

    /**
     * @ORM\Column(type="text")
     * @Groups({"Coaching:read"})
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity=Sport::class, inversedBy="coaches")
     * @Groups({"Coaching:read"})
     */
    private $sports;

    public function __construct()
    {
        $this->sports = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getRoles(): array
    {
        return ['ROLE_USER', 'ROLE_COACH'];
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Sport>
     */
    public function getSports(): Collection
    {
        return $this->sports;
    }

    public function addSport(Sport $sport): self
    {
        if (!$this->sports->contains($sport)) {
            $this->sports[] = $sport;
        }

        return $this;
    }

    public function removeSport(Sport $sport): self
    {
        $this->sports->removeElement($sport);

        return $this;
    }
}
