<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ComplexPictureRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     formats={"json"},
 *     normalizationContext={"groups"={"ComplexPicture:read"}},
 *     denormalizationContext={"groups"={"ComplexPicture:write"}}
 * )
 * @ORM\Entity(repositoryClass=ComplexPictureRepository::class)
 */
class ComplexPicture
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"ComplexPicture:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"Complex:read", "ComplexPicture:read", "ComplexPicture:write", "Complex:write"})
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity=Complex::class, inversedBy="complexPictures")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"ComplexPicture:read", "ComplexPicture:write"})
     */
    public $complex;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getComplex(): ?Complex
    {
        return $this->complex;
    }

    public function setComplex(?Complex $complex): self
    {
        $this->complex = $complex;

        return $this;
    }
}
