<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\TimestampableTrait;
use App\Repository\FriendshipRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(formats={"json"},
 *     normalizationContext={"groups"={"Friendship:read"}},
 *     denormalizationContext={"groups"={"Friendship:write"}}
 * )
 * @ORM\Entity(repositoryClass=FriendshipRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Friendship
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({
     *     "Friendship:read",
     *     "User:read"
     * })
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({
     *     "Friendship:read",
     *     "Friendship:write",
     * })
     */
    private $isFriend;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="friendships")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     "Friendship:read",
     *     "User:read"
     * })
     */
    public $sender;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="friendships")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     "Friendship:read",
     *     "Friendship:write",
     *     "User:read"
     * })
     */
    public $receiver;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsFriend(): ?bool
    {
        return $this->isFriend;
    }

    public function setIsFriend(bool $isFriend): self
    {
        $this->isFriend = $isFriend;

        return $this;
    }

    public function getSender(): ?User
    {
        return $this->sender;
    }

    public function setSender(?User $sender): self
    {
        $this->sender = $sender;

        return $this;
    }

    public function getReceiver(): ?User
    {
        return $this->receiver;
    }

    public function setReceiver(?User $receiver): self
    {
        $this->receiver = $receiver;

        return $this;
    }
}
