<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ReviewRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     formats={"json"},
 *     normalizationContext={"groups"={"Review:read"}},
 *     denormalizationContext={"groups"={"Review:write"}}
 * )
 * @ORM\Entity(repositoryClass=ReviewRepository::class)
 */
class Review
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({
     *      "Complex:read",
     *      "Review:read"
     * })
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({
     *      "Complex:read",
     *      "Review:read",
     *      "Review:write"
     * })
     */
    private $rate;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({
     *      "Complex:read",
     *      "Review:read",
     *      "Review:write"
     * })
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity=Complex::class, inversedBy="reviews")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *      "Review:read",
     *      "Review:write"
     * })
     */
    private $complex;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="reviews")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *      "Complex:read",
     *      "Review:read"
     * })
     */
    public $reviewer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRate(): ?int
    {
        return $this->rate;
    }

    public function setRate(int $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getComplex(): ?Complex
    {
        return $this->complex;
    }

    public function setComplex(?Complex $complex): self
    {
        $this->complex = $complex;

        return $this;
    }

    public function getReviewer(): ?User
    {
        return $this->reviewer;
    }

    public function setReviewer(?User $reviewer): self
    {
        $this->reviewer = $reviewer;

        return $this;
    }
}
