<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SportRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     formats={"json"},
 *     normalizationContext={"groups"={"Sport:read"}},
 *     denormalizationContext={"groups"={"Sport:write"}}
 * )
 * @ORM\Entity(repositoryClass=SportRepository::class)
 */
class Sport
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({
     *      "Coaching:read",
     *      "Sport:read",
     *      "Complex:read"
     * })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *      "Coaching:read",
     *      "Complex:read",
     *      "Land:read",
     *      "Sport:read",
     *      "User:read",
     *      "Sport:write"
     * })
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Groups({
     *      "Coaching:read",
     *      "Land:read",
     *      "Sport:read",
     *      "Sport:write"
     * })
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Land::class, mappedBy="sport", orphanRemoval=true)
     * @Groups({"Sport:read"})
     */
    private $lands;

    /**
     * @ORM\OneToMany(targetEntity=UserSport::class, mappedBy="sport")
     */
    private $userSports;

    /**
     * @ORM\ManyToMany(targetEntity=Coach::class, mappedBy="sports")
     */
    private $coaches;

    public function __construct()
    {
        $this->lands = new ArrayCollection();
        $this->userSports = new ArrayCollection();
        $this->coaches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Land>
     */
    public function getLands(): Collection
    {
        return $this->lands;
    }

    public function addLand(Land $land): self
    {
        if (!$this->lands->contains($land)) {
            $this->lands[] = $land;
            $land->setSport($this);
        }

        return $this;
    }

    public function removeLand(Land $land): self
    {
        if ($this->lands->removeElement($land)) {
            // set the owning side to null (unless already changed)
            if ($land->getSport() === $this) {
                $land->setSport(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, UserSport>
     */
    public function getUserSports(): Collection
    {
        return $this->userSports;
    }

    public function addUserSport(UserSport $userSport): self
    {
        if (!$this->userSports->contains($userSport)) {
            $this->userSports[] = $userSport;
            $userSport->setSport($this);
        }

        return $this;
    }

    public function removeUserSport(UserSport $userSport): self
    {
        if ($this->userSports->removeElement($userSport)) {
            // set the owning side to null (unless already changed)
            if ($userSport->getSport() === $this) {
                $userSport->setSport(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Coach>
     */
    public function getCoaches(): Collection
    {
        return $this->coaches;
    }

    public function addCoach(Coach $coach): self
    {
        if (!$this->coaches->contains($coach)) {
            $this->coaches[] = $coach;
            $coach->addSport($this);
        }

        return $this;
    }

    public function removeCoach(Coach $coach): self
    {
        if ($this->coaches->removeElement($coach)) {
            $coach->removeSport($this);
        }

        return $this;
    }
}
