<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     formats={"json"},
 *     normalizationContext={"groups"={"User:read"}},
 *     denormalizationContext={"groups"={"User:write"}}
 * )
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    use TimestampableTrait;

    public static $OPERATION_GET = "get";

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({
     *      "Complex:read",
     *      "Coaching:read",
     *      "User:read"
     * })
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({
     *      "Booking:read",
     *      "Coaching:read",
     *      "Land:read",
     *      "User:read",
     *      "User:write"
     * })
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * @Groups({
     *      "Booking:read",
     *      "Coaching:read",
     *      "User:write",
     *      "User:read",
     * })
     */

    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups({"User:write"})
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "Booking:read",
     *     "Complex:read",
     *     "Friendship:read",
     *     "Coaching:read",
     *     "Land:read",
     *     "Review:read",
     *     "User:read",
     *     "User:write"
     * })
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "Booking:read",
     *     "Complex:read",
     *     "Friendship:read",
     *     "Coaching:read",
     *     "Land:read",
     *     "Review:read",
     *     "User:read",
     *     "User:write"
     * })
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *      "Coaching:read",
     *      "User:read",
     *      "User:write",
     *      "Booking:read",
     * })
     */
    private $phone;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({
     *      "Coaching:read",
     *      "User:read",
     *      "User:write"
     * })
     */
    private $isPublic;

    /**
     * @ORM\OneToMany(targetEntity=Review::class, mappedBy="reviewer", orphanRemoval=true)
     * @Groups({"Coaching:read"})
     */
    private $reviews;

    /**
     * @ORM\ManyToMany(targetEntity=Complex::class, inversedBy="users")
     * @Groups({"Coaching:read"})
     */
    private $manager;

    /**
     * @ORM\ManyToMany(targetEntity=Booking::class, mappedBy="participants")
     * @Groups({"Coaching:read"})
     */
    private $bookings;

    /**
     * @ORM\OneToMany(targetEntity=Friendship::class, mappedBy="sender")
     * @Groups({
     *      "Coaching:read"
     * })
     */
    private $friendships;

    /**
     * @ORM\OneToMany(targetEntity=Friendship::class, mappedBy="receiver")
     */
    private $friendshipsReceiver;

    /**
     * @ORM\OneToMany(targetEntity=UserSport::class, mappedBy="player", orphanRemoval=true, cascade={"persist"})
     * @Groups({
     *      "Coaching:read",
     *      "Coaching:read",
     *      "User:read",
     *      "User:write"
     * })
     */
    private $userSports;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "Booking:read",
     *     "Complex:read",
     *     "Friendship:read",
     *     "Land:read",
     *     "Review:read",
     *     "User:read",
     *     "User:write"
     * })
     */
    private $avatarUrl;

    /**
     * @Groups({
     *     "User:read"
     * })
     */
    private $friends;

    public function __construct()
    {
        $this->reviews = new ArrayCollection();
        $this->manager = new ArrayCollection();
        $this->bookings = new ArrayCollection();
        $this->friendships = new ArrayCollection();
        $this->userSports = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string)$this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getIsPublic(): ?bool
    {
        return $this->isPublic;
    }

    public function setIsPublic(bool $isPublic): self
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * @return Collection<int, Review>
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    public function addReview(Review $review): self
    {
        if (!$this->reviews->contains($review)) {
            $this->reviews[] = $review;
            $review->setReviewer($this);
        }

        return $this;
    }

    public function removeReview(Review $review): self
    {
        if ($this->reviews->removeElement($review)) {
            // set the owning side to null (unless already changed)
            if ($review->getReviewer() === $this) {
                $review->setReviewer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Complex>
     */
    public function getManager(): Collection
    {
        return $this->manager;
    }

    public function addManager(Complex $manager): self
    {
        if (!$this->manager->contains($manager)) {
            $this->manager[] = $manager;
        }

        return $this;
    }

    public function removeManager(Complex $manager): self
    {
        $this->manager->removeElement($manager);

        return $this;
    }

    /**
     * @return Collection<int, Booking>
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->addParticipant($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->removeElement($booking)) {
            $booking->removeParticipant($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Friendship>
     */
    public function getFriendships(): Collection
    {
        return $this->friendships;
    }

    public function addFriendship(Friendship $friendship): self
    {
        if (!$this->friendships->contains($friendship)) {
            $this->friendships[] = $friendship;
            $friendship->setSender($this);
        }

        return $this;
    }

    public function removeFriendship(Friendship $friendship): self
    {
        if ($this->friendships->removeElement($friendship)) {
            // set the owning side to null (unless already changed)
            if ($friendship->getSender() === $this) {
                $friendship->setSender(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, UserSport>
     */
    public function getUserSports(): Collection
    {
        return $this->userSports;
    }

    public function addUserSport(UserSport $userSport): self
    {
        if (!$this->userSports->contains($userSport)) {
            $this->userSports[] = $userSport;
            $userSport->setPlayer($this);
        }

        return $this;
    }

    public function removeUserSport(UserSport $userSport): self
    {
        if ($this->userSports->removeElement($userSport)) {
            // set the owning side to null (unless already changed)
            if ($userSport->getPlayer() === $this) {
                $userSport->setPlayer(null);
            }
        }

        return $this;
    }

    public function getAvatarUrl(): ?string
    {
        return $this->avatarUrl;
    }

    public function setAvatarUrl(string $avatarUrl): self
    {
        $this->avatarUrl = $avatarUrl;

        return $this;
    }

    public function getFriends(): ?array
    {
        $friends = [];
        /* @var $friendship Friendship */
        foreach ($this->friendshipsReceiver as $friendship) {
            if($friendship->getIsFriend()){
                $friends[] = $friendship->getSender();
            }
        }
        foreach ($this->friendships as $friendship) {
            if($friendship->getIsFriend()){
                $friends[] = $friendship->getReceiver();
            }
        }

        return $friends;
    }

    public function setFriends(array $friends): self
    {
        $this->friends = $friends;

        return $this;

    }
}
