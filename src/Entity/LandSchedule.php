<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\LandScheduleRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     formats={"json"},
 *     normalizationContext={"groups"={"LandSchedule:read"}},
 *     denormalizationContext={"groups"={"LandSchedule:write"}}
 * )
 * @ORM\Entity(repositoryClass=LandScheduleRepository::class)
 */
class LandSchedule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"LandSchedule:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({
     *     "LandSchedule:read",
     *     "Land:read",
     *     "Complex:write",
     *     "LandSchedule:write",
     *     "Land:write",
     *     "Complex:read"
     * })
     */
    private $startingTimeMorning;

    /**
     * @ORM\Column(type="integer")
     * @Groups({
     *     "LandSchedule:read",
     *     "Land:read",
     *     "Complex:write",
     *     "LandSchedule:write",
     *     "Land:write",
     *     "Complex:read"
     * })
     */
    private $endingTimeMorning;

    /**
     * @ORM\Column(type="integer")
     * @Groups({
     *     "LandSchedule:read",
     *     "Land:read",
     *     "Complex:write",
     *     "LandSchedule:write",
     *     "Land:write",
     *     "Complex:read"
     * })
     */
    private $startingTimeAfternoon;

    /**
     * @ORM\Column(type="integer")
     * @Groups({
     *     "LandSchedule:read",
     *     "Land:read",
     *     "Complex:write",
     *     "LandSchedule:write",
     *     "Land:write",
     *     "Complex:read"
     * })
     */
    private $endingTimeAfternoon;

    /**
     * @ORM\ManyToOne(targetEntity=Land::class, inversedBy="landSchedules", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     "LandSchedule:read",
     *     "LandSchedule:write"
     * })
     */
    public $land;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "LandSchedule:read",
     *     "Land:read",
     *     "Complex:write",
     *     "LandSchedule:write",
     *     "Land:write",
     *     "Complex:read"
     * })
     */
    private $day;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartingTimeMorning(): ?int
    {
        return $this->startingTimeMorning;
    }

    public function setStartingTimeMorning(int $startingTimeMorning): self
    {
        $this->startingTimeMorning = $startingTimeMorning;

        return $this;
    }

    public function getEndingTimeMorning(): ?int
    {
        return $this->endingTimeMorning;
    }

    public function setEndingTimeMorning(int $endingTimeMorning): self
    {
        $this->endingTimeMorning = $endingTimeMorning;

        return $this;
    }

    public function getStartingTimeAfternoon(): ?int
    {
        return $this->startingTimeAfternoon;
    }

    public function setStartingTimeAfternoon(int $startingTimeAfternoon): self
    {
        $this->startingTimeAfternoon = $startingTimeAfternoon;

        return $this;
    }

    public function getEndingTimeAfternoon(): ?int
    {
        return $this->endingTimeAfternoon;
    }

    public function setEndingTimeAfternoon(int $endingTimeAfternoon): self
    {
        $this->endingTimeAfternoon = $endingTimeAfternoon;

        return $this;
    }

    public function getLand(): ?Land
    {
        return $this->land;
    }

    public function setLand(?Land $land): self
    {
        $this->land = $land;

        return $this;
    }

    public function getDay(): ?string
    {
        return $this->day;
    }

    public function setDay(string $day): self
    {
        $this->day = $day;

        return $this;
    }
    // public function __toString()
    // {
    //     return $this->day;
    // }
}
