<?php

namespace App\Repository;

use App\Entity\Complex;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Complex|null find($id, $lockMode = null, $lockVersion = null)
 * @method Complex|null findOneBy(array $criteria, array $orderBy = null)
 * @method Complex[]    findAll()
 * @method Complex[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComplexRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Complex::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Complex $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Complex $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return Complex[] Returns an array of Complex objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Complex
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findByFilters($filters)
    {
        $pageSize = '8';

        $query = $this->createQueryBuilder('c');
        $query->join('c.lands', 'l');
        $query->join('l.sport', 's');
        $query->join('c.users', 'u');

        if (isset($filters['name'])) {
            $query->andWhere('c.name LIKE :name')
                ->setParameter('name', "%" . $filters['name'] . "%");
        }

        if (isset($filters['postal'])) {
            $query->andWhere('c.postal = :postal')
                ->setParameter('postal', preg_replace('/\s+/', '', $filters['postal']));
        }

        if (isset($filters['city'])) {
            $query->andWhere('c.city = :city')
                ->setParameter('city', $filters['city']);
        }

        if (isset($filters['sports'])) {
            $query->andWhere("s.id IN(:sports)");
            $query->setParameter('sports', $filters['sports']);
        }

        if (isset($filters['hasChangingRoom'])) {
            $query->andWhere('c.hasChangingRoom = :hasChangingRoom')
                ->setParameter('hasChangingRoom', $filters['hasChangingRoom']);
        }

        if (isset($filters['manager'])) {
            $query->andWhere(":manager MEMBER OF c.users");
            $query->setParameter('manager', $filters['manager']);
        }

        $paginator = new Paginator($query);
        $totalCount = count($paginator);

        $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($filters['page'] - 1))
            ->setMaxResults($pageSize);

        return ['complexes' => $paginator, 'totalCount' => $totalCount];
    }
}
