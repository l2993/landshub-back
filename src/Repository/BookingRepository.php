<?php

namespace App\Repository;

use App\Entity\Booking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Booking::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Booking $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Booking $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return Booking[] Returns an array of Booking objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Booking
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function resamax($day, $idUser): int
    {


        $query = $this->createQueryBuilder('b')
        ->join('b.participants', 'p')
        ->andWhere('b.bookedTime BETWEEN :from AND :to')
        ->andWhere('b.isCancelled = false')
        ->andWhere('p.id = :user')
        ->setParameter('from', $day->format('Y-m-d') . ' 00:00:00')
        ->setParameter('to', $day->format('Y-m-d') . ' 23:59:59')
        ->setParameter('user', $idUser)
        ->getQuery()
        ->getResult()
        ;
        if (count($query) >= 4) {
            return true;
        } else {
            return false;
        }
    }

    public function findByFilters($filters)
    {
        $pageSize = '8';

        $query = $this->createQueryBuilder('b');
        $query->join('b.land', 'l');

        if (isset($filters['date'])) {
            $date = \DateTime::createFromFormat('d/m/Y', $filters['date']);

            $query
                ->andWhere('b.bookedTime >= :date_start')
                ->andWhere('b.bookedTime <= :date_end')
                ->setParameter('date_start', $date->format('Y-m-d 00:00:00'))
                ->setParameter('date_end', $date->format('Y-m-d 23:59:59'));
        }

        if (isset($filters['land'])) {
            $query
                ->andWhere('l.id = :idLand')
                ->setParameter('idLand', $filters['land']);
        }

        $paginator = new Paginator($query);
        $totalCount = count($paginator);

        if (!isset($filters['page'])) {
            $filters['page'] = 1;
        }
        $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($filters['page'] - 1))
            ->setMaxResults($pageSize);

        return ['bookings' => $paginator, 'totalCount' => $totalCount];
    }
}
