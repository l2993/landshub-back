<?php

namespace App\Controller;

use App\Entity\Land;
use App\Repository\LandRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LandController extends AbstractController
{
    /**
     * @Route(
     *     path="/api/land_schedules/land/{id}",
     *     methods={"GET"},
     *     defaults={
     *         "_api_resource_class"=Land::class
     *     }
     * )
     * @param Request $request
     * @param LandRepository $landRepository
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function getLandSchedules(
        Request $request,
        LandRepository $landRepository,
        SerializerInterface $serializer
    ): Response {
        $land = $landRepository->find($request->get("id"));

        if (!is_null($land)) {
            $schedules = $land->getLandSchedules();
            $response = $serializer->serialize($schedules, 'json', ['groups' => 'LandSchedule:read']);
            return new Response($response);
        }

        throw new NotFoundHttpException("Land not found");
    }
}
