<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Service\Mailer;
use App\Repository\BookingRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BookingController extends AbstractController
{

    /**
     * Service Security
     *
     * @var [Security]
     */
    private $security;
    private $mailer;

    public function __construct(Security $security, Mailer $mailer)
    {
        $this->security = $security;
        $this->mailer = $mailer;
    }
    /**
     * @Route(
     *     path="/api/booking/{id}/participants",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=Booking::class
     *     }
     * )
     * @param Request $request
     * @param BookingRepository $bookingRepository
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function addParticipants(
        Request $request,
        BookingRepository $bookingRepository,
        SerializerInterface $serializer
    ): Response {
        $booking = $bookingRepository->find($request->get("id"));

        if (!is_null($booking)) {
            if (!in_array($this->security->getUser(), $booking->getParticipantList())) {
                $booking->addParticipant($this->security->getUser());
                $this->mailer->sendEmail(
                    $this->security->getUser(),
                    "LandsHub | Confirmation de votre réservation !",
                    "reservation_accepted",
                    ["booking" => $booking,"user" => $this->security->getUser()]
                );

                $response = $serializer->serialize($booking, 'json', ['groups' => 'Booking:read']);
                return new Response($response);
            } else {
                throw new NotFoundHttpException("You are already in the booking");
            }
        }

        throw new NotFoundHttpException("Booking not found");
    }
}
