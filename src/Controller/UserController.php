<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends AbstractController
{
    /**
     * Service Security
     *
     * @var [Security]
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route(
     *     path="/api/profile",
     *     methods={"GET"},
     *     defaults={
     *         "_api_resource_class"=User::class
     *     }
     * )
     * @param Request $request
     * @param UserRepository $complexRepository
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function getProfil(
        Request $request,
        UserRepository $userRepository,
        SerializerInterface $serializer
    ): Response {
        $userConnected = $this->security->getUser();

        if (is_null($userConnected)) {
            throw new NotFoundHttpException("JWT Token not found");
        } else {
            $id = $userConnected->getId();
            $user = $userRepository->find($id);

            if (!is_null($user)) {
                $response = $serializer->serialize($user, 'json', ['groups' => 'User:read']);
                return new Response($response);
            }

            throw new NotFoundHttpException("User not found");
        }
    }
}
