<?php

namespace App\Controller;

use App\Entity\Complex;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\ComplexRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ComplexController extends AbstractController
{
    /**
     * @Route(
     *     path="/api/complex_pictures/complex/{id}",
     *     methods={"GET"},
     *     defaults={
     *         "_api_resource_class"=Complex::class
     *     }
     * )
     * @param Request $request
     * @param ComplexRepository $complexRepository
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function getComplexPictures(
        Request $request,
        ComplexRepository $complexRepository,
        SerializerInterface $serializer
    ): Response {
        $complex = $complexRepository->find($request->get("id"));

        if (!is_null($complex)) {
            $pictures = $complex->getComplexPictures();
            $response = $serializer->serialize($pictures, 'json', ['groups' => 'ComplexPicture:read']);
            return new Response($response);
        }

        throw new NotFoundHttpException("Complex not found");
    }

    /**
     * @Route(
     *     path="/api/lands/complex/{id}",
     *     methods={"GET"},
     *     defaults={
     *         "_api_resource_class"=Complex::class
     *     }
     * )
     * @param Request $request
     * @param ComplexRepository $complexRepository
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function getComplexLands(
        Request $request,
        ComplexRepository $complexRepository,
        SerializerInterface $serializer
    ): Response {
        $complex = $complexRepository->find($request->get("id"));

        if (!is_null($complex)) {
            $lands = $complex->getLands();
            $response = $serializer->serialize($lands, 'json', ['groups' => 'Land:read']);
            return new Response($response);
        }

        throw new NotFoundHttpException("Complex not found");
    }

    /**
     * @Route(
     *     path="/api/reviews/complex/{id}",
     *     methods={"GET"},
     *     defaults={
     *         "_api_resource_class"=Complex::class
     *     }
     * )
     * @param Request $request
     * @param ComplexRepository $complexRepository
     * @return Response
     */
    public function getComplexReview(
        Request $request,
        ComplexRepository $complexRepository,
        SerializerInterface $serializer
    ): Response {
        $complex = $complexRepository->find($request->get("id"));

        if (!is_null($complex)) {
            $reviews = $complex->getReviews();
            $response = $serializer->serialize($reviews, 'json', ['groups' => 'Review:read']);
            return new Response($response);
        }

        throw new NotFoundHttpException("Complex not found");
    }

    /**
     * @Route(
     *     path="/api/complexes",
     *     methods={"GET"},
     *     defaults={
     *         "_api_resource_class"=Complex::class
     *     }
     * )
     * @param Request $request
     * @param UserRepository $userRepository
     * @param ComplexRepository $complexRepository
     * @param SerializerInterface $serializer
     * @param Security $security
     * @return Response
     */
    public function getComplexes(
        Request $request,
        UserRepository $userRepository,
        ComplexRepository $complexRepository,
        SerializerInterface $serializer,
        Security $security
    ): Response {

        $userConnected = $security->getUser();

        if (is_null($userConnected)) {
            $response = $serializer->serialize($complexRepository->findByFilters($_GET), 'json', ['groups' => 'Complex:read']);
            return new Response($response);
        } elseif (in_array("ROLE_MANAGER", $userConnected->getRoles())) {
            $manager = $userRepository->find($userConnected->getId());

            $complexes = [];
            $_GET['manager'] = $manager;
            $response = $serializer->serialize($complexRepository->findByFilters($_GET), 'json', ['groups' => 'Complex:read']);
            return new Response($response);
        } else {
            $response = $serializer->serialize($complexRepository->findByFilters($_GET), 'json', ['groups' => 'Complex:read']);
            return new Response($response);
        }
    }
}
